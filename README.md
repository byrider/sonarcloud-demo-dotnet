# Sample .NET project

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=byrider_sonarcloud-demo-dotnet&metric=alert_status)](https://sonarcloud.io/dashboard?id=byrider_sonarcloud-demo-dotnet)

This project is built and analyzed on SonarCloud with [SonarCloud-Scan Bitbucket Pipe](https://bitbucket.org/sonarsource/sonarcloud-scan/src/master/) to trigger the analysis. A Bitbucket Pipeline is configured to be triggered upon both pushes on master branch and on PR analysis (with provided bitbucket-pipelines.yml file)
